const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const User = require('../model/user')
const bcrypt = require('bcrypt');



router.post('/signup', (req, res, next)=>{
    User.find({email: req.body.email}).exec().then(
    user =>{
        if(user.length >= 1){
      return res.status(409).json({
        message:'User already exist!'
    })
        }
        else{
        bcrypt.hash(req.body.password, 10,(err, hash)=>{
            if(err){
                return res.status(500).json({
           error: err               
                });
            }
    
        else{
    
            const user = new User({
                _id:new mongoose.Types.ObjectId,
                email: req.body.email,
                password:hash
            
            })
            user.save().then(result=>{
                console.log(result);
                res.status(201).json({
                    message: 'User created successfully!'
                })
            }).catch(err=>{
    res.status(500).json({
        error: err
    });
    
            })
              
        } 
    }); 
    
        }
    }
    
    ).catch();
    
    
    
    });

    

    router.post('/login', async (req, res) => {
        var newUser = {};
        newUser.email = req.body.email;
        newUser.password = req.body.password;
      
        await User.findOne({ email: newUser.email })
          .then(profile => {
            if (!profile) {
              res.send("User not exist");
            } else {
              bcrypt.compare(
                newUser.password,
                profile.password,
                async (err, result) => {
                  if (err) {
                    console.log("Error is", err.message);
                  } else if (result == true) {
                    res.send("User authenticated");
                  } else {
                    res.send("User Unauthorized Access");
                  }
                }
              );
            }
          })
          .catch(err => {
            console.log("Error is ", err.message);
          });
      });
    

module.exports = router;