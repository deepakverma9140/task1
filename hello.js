const { graphql, buildSchema } = require('graphql');

const express = require('express');
const { graphqlHTTP } = require('express-graphql');


const schema = buildSchema(`
  type Query {
    hello: String
  }
`);

const root = { hello: () => 'Hello world!' };

const app = express();
app.use('/graphql', graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true,
  }));
  app.listen(3000, () => console.log('Now browse to localhost:3000/graphql'));

// graphql(schema, '{ hello }', root).then((response) => {
//   console.log(response);
// });